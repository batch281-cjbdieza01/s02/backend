/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

console.log("Hello World!");
let firstName2 = "CJ";
console.log("First Name: " + firstName2);
let	lastName2 = "Dieza";
console.log("Last Name: " + lastName2);
let age = 30;
console.log("Age: " + age);
let hobbies = ["Playing Guitar", "Road Trip", "Mobile Gaming"];
console.log("Hobbies:");
console.log(hobbies);


let workAddress = {
	bldgNumber: '0088',
	street : 'Burgos Street',
	city : 'Labo',
	state : 'Camarines Norte'

}
console.log("My Work Address:");
console.log(workAddress);



	let firstName = "Steve";
	console.log("My First Name is: " +  firstName);

	let lastName = " Rogers"
	console.log("my Last Name is : " + lastName)

	let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let age2 = 40;
	console.log("My current age is: " + age2);
	
	let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
	
	console.log("My Friends are: ");
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let bestfriend = "Bucky Barnes";
	console.log("My bestfriend is: " + bestfriend);

	const lastLocation = "Arctic Ocean";
	//lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);